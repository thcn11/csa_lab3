import os.path
import tempfile
import pytest

import processor
import translator

from common_functions import *


@pytest.mark.golden_test('tests/*.yml')
def test_golden(golden, caplog):
    with tempfile.TemporaryDirectory() as tmp_dir:
        output_file: str = os.path.join(tmp_dir, 'compiled.yaml')
        translator.translate(golden['input_file'], output_file)
        proc: processor.ProcessorController = processor.ProcessorController()
        proc.load_programm(output_file)
        if golden['input_data'] is not None:
            splitted: list = golden['input_data'].split(',')
            for word in splitted:
                word_st: str = word.strip()
                if is_int(word_st):
                    proc.write_input_int(int(word_st, 0))
                else:
                    proc.write_input(word_st)
        proc.work()
        assert proc.get_all_output() == golden.out['output']
