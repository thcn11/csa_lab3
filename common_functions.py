
def is_int(s: str):
    try:
        int(s, 0)
        return True
    except ValueError:
        return False
