import processor
from microprogram import *
from masks import *


class ControlUnit():
    def __init__(self):
        self.ip: int = 0

        self.cr: int = 0

        self.proc: processor.Processor = processor.Processor()

        self.microprogram = microprogram

    def tick(self):
        vertical_mc: str = self.microprogram[self.ip]
        self.cr = self.decode_command(vertical_mc)
        self.ip += 1
        if self.cr & CONTROL_BIT:
            command_without_mask_and_address = self.cr & ~MASK_BITS & ~ADDRESS_BITS
            self.proc.execute_operational_mc(command_without_mask_and_address)
            mask: int = (self.cr & MASK_BITS) >> 40
            address: int = (self.cr & ADDRESS_BITS) >> 30
            if self.proc.out & mask:
                self.ip = address
        else:
            self.proc.execute_operational_mc(self.cr)

    def decode_command(self, vertical_micro_command: str) -> int:
        decoded: int = 0

        if vertical_micro_command[0] == '1':
            decoded |= CONTROL_BIT
            decoded |= int(vertical_micro_command[15:17], 16) << 40
            decoded |= int(vertical_micro_command[17:20], 16) << 30

        if vertical_micro_command[1] == '1':
            decoded |= MASKS[RA_OUT1]
        elif vertical_micro_command[1] == '2':
            decoded |= MASKS[RB_OUT1]
        elif vertical_micro_command[1] == '3':
            decoded |= MASKS[RC_OUT1]
        elif vertical_micro_command[1] == '4':
            decoded |= MASKS[RD_OUT1]
        elif vertical_micro_command[1] == '5':
            decoded |= MASKS[BUF_OUT]
        elif vertical_micro_command[1] == '6':
            decoded |= MASKS[PS_OUT]

        if vertical_micro_command[2] == '1':
            decoded |= MASKS[RA_OUT2]
        elif vertical_micro_command[2] == '2':
            decoded |= MASKS[RB_OUT2]
        elif vertical_micro_command[2] == '3':
            decoded |= MASKS[RC_OUT2]
        elif vertical_micro_command[2] == '4':
            decoded |= MASKS[RD_OUT2]
        elif vertical_micro_command[2] == '5':
            decoded |= MASKS[CR_OUT]
        elif vertical_micro_command[2] == '6':
            decoded |= MASKS[SP_OUT]
        elif vertical_micro_command[2] == '7':
            decoded |= MASKS[IP_OUT]
        elif vertical_micro_command[2] == '8':
            decoded |= MASKS[DATA_OUTPUT]

        if vertical_micro_command[3] == '1':
            decoded |= MASKS[RA_INPUT]
        elif vertical_micro_command[3] == '2':
            decoded |= MASKS[RB_INPUT]
        elif vertical_micro_command[3] == '3':
            decoded |= MASKS[RC_INPUT]
        elif vertical_micro_command[3] == '4':
            decoded |= MASKS[RD_INPUT]
        elif vertical_micro_command[3] == '5':
            decoded |= MASKS[CR_INPUT]
        elif vertical_micro_command[3] == '6':
            decoded |= MASKS[IP_INPUT]
        elif vertical_micro_command[3] == '7':
            decoded |= MASKS[SP_INPUT]
        elif vertical_micro_command[3] == '8':
            decoded |= MASKS[BUF_INPUT]
        elif vertical_micro_command[3] == '9':
            decoded |= MASKS[DATA_INPUT]
        elif vertical_micro_command[3] == 'A':
            decoded |= MASKS[ADDR_INPUT]

        if vertical_micro_command[4] == '0':
            decoded |= MASKS[ALU_SUM]
        elif vertical_micro_command[4] == '1':
            decoded |= MASKS[ALU_MUL]
        elif vertical_micro_command[4] == '2':
            decoded |= MASKS[ALU_DIV]

        if vertical_micro_command[5] == '1':
            decoded |= MASKS[ALU_LEFT_INV]

        if vertical_micro_command[6] == '1':
            decoded |= MASKS[ALU_RIGHT_INV]

        if vertical_micro_command[7] == '1':
            decoded |= MASKS[ALU_INC]

        if vertical_micro_command[8] == '1':
            decoded |= MASKS[COM_L_TO_L]
            decoded |= MASKS[COM_H_TO_H]
        elif vertical_micro_command[8] == '2':
            decoded |= MASKS[COM_L_TO_H]
            decoded |= MASKS[COM_H_TO_L]
        elif vertical_micro_command[8] == '3':
            decoded |= MASKS[COM_L_TO_L]
        elif vertical_micro_command[8] == '4':
            decoded |= MASKS[COM_H_TO_L]

        if vertical_micro_command[9] == '1':
            decoded |= MASKS[COM_SHIFT_L]
        elif vertical_micro_command[9] == '2':
            decoded |= MASKS[COM_SHIFT_L]
            decoded |= MASKS[COM_SHIFT_L_CYCLE]
        elif vertical_micro_command[9] == '3':
            decoded |= MASKS[COM_SHIFT_R]
        elif vertical_micro_command[9] == '4':
            decoded |= MASKS[COM_SHIFT_R]
            decoded |= MASKS[COM_SHIFT_R_CYCLE]

        if vertical_micro_command[10] == '1':
            decoded |= MASKS[SET_C]

        if vertical_micro_command[11] == '1':
            decoded |= MASKS[SET_V]

        if vertical_micro_command[12] == '1':
            decoded |= MASKS[SET_N]

        if vertical_micro_command[13] == '1':
            decoded |= MASKS[SET_Z]

        if vertical_micro_command[14] == '1':
            decoded |= MASKS[WRITE_TO_MEM]
        elif vertical_micro_command[14] == '2':
            decoded |= MASKS[READ_FROM_MEM]
        elif vertical_micro_command[14] == '3':
            decoded |= MASKS[WRITE_TO_COMMAND_MEM]
        elif vertical_micro_command[14] == '4':
            decoded |= MASKS[READ_FROM_COMMAND_MEM]
        elif vertical_micro_command[14] == '5':
            decoded |= MASKS[OUTPUT]
        elif vertical_micro_command[14] == '6':
            decoded |= MASKS[INPUT]
        elif vertical_micro_command[14] == '7':
            decoded |= MASKS[HALT]

        return decoded

    def is_processor_working(self) -> bool:
        return self.proc.ps & PS_IS_WORKING
