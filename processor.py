import argparse
import io
import json

from masks import *
from control_unit import *
from common_functions import *


class Processor:
    def __init__(self):
        # Регистры
        self.cr: int = 0
        self.ra: int = 0
        self.rb: int = 0
        self.rc: int = 0
        self.rd: int = 0
        self.ip: int = 0
        self.sp: int = 0
        self.ps: int = PS_ALWAYS_1
        self.buf: int = 0
        self.data: int = 0
        self.addr: int = 0

        # Шины
        self.oper_l: int = 0
        self.oper_r: int = 0
        self.alu_to_com: int = 0
        self.out: int = 0

        # Память
        self.data_mem: list[int] = [0 for i in range(128)]
        self.command_mem: list[int] = [0 for i in range(128)]

        # Oчередь ввода:
        self.input_queue: list[int] = []

        # Очередь вывода
        self.out_queue: list[int] = []

    def use_data_input(self, microcom: int):
        if microcom & MASKS[READ_FROM_MEM]:
            self.data = self.data_mem[self.addr]

        if microcom & MASKS[READ_FROM_COMMAND_MEM]:
            self.data = self.command_mem[self.addr]

        if microcom & MASKS[INPUT]:
            if len(self.input_queue) > 0:
                self.data = self.input_queue.pop()
            else:
                assert 'Trying to read from empty queue'

    def use_data_output(self, microcom: int):
        if microcom & MASKS[WRITE_TO_MEM]:
            self.data_mem[self.addr] = self.data

        if microcom & MASKS[WRITE_TO_COMMAND_MEM]:
            self.command_mem[self.addr] = self.data

        if microcom & MASKS[OUTPUT]:
            self.out_queue.insert(0, self.data)

    def fill_oper_l(self, microcom):
        if microcom & MASKS[RA_OUT1]:
            if self.oper_l != 0:
                assert 'Error, someone already wrote to bus'
            self.oper_l = self.ra

        if microcom & MASKS[RB_OUT1]:
            if self.oper_l != 0:
                assert 'Error, someone already wrote to bus'
            self.oper_l = self.rb

        if microcom & MASKS[RC_OUT1]:
            if self.oper_l != 0:
                assert 'Error, someone already wrote to bus'
            self.oper_l = self.rc

        if microcom & MASKS[RD_OUT1]:
            if self.oper_l != 0:
                assert 'Error, someone already wrote to bus'
            self.oper_l = self.rd

        if microcom & MASKS[BUF_OUT]:
            if self.oper_l != 0:
                assert 'Error, someone already wrote to bus'
            self.oper_l = self.buf

        if microcom & MASKS[PS_OUT]:
            if self.oper_l != 0:
                assert 'Error, someone already wrote to bus'
            self.oper_l = self.ps

    def fill_oper_r(self, microcom):
        if microcom & MASKS[CR_OUT]:
            if self.oper_r != 0:
                assert 'Error, someone already wrote to bus'
            self.oper_r = self.cr

        if microcom & MASKS[RA_OUT2]:
            if self.oper_r != 0:
                assert 'Error, someone already wrote to bus'
            self.oper_r = self.ra

        if microcom & MASKS[RB_OUT2]:
            if self.oper_r != 0:
                assert 'Error, someone already wrote to bus'
            self.oper_r = self.rb

        if microcom & MASKS[RC_OUT2]:
            if self.oper_r != 0:
                assert 'Error, someone already wrote to bus'
            self.oper_r = self.rc

        if microcom & MASKS[RD_OUT2]:
            if self.oper_r != 0:
                assert 'Error, someone already wrote to bus'
            self.oper_r = self.rd

        if microcom & MASKS[SP_OUT]:
            if self.oper_r != 0:
                assert 'Error, someone already wrote to bus'
            self.oper_r = self.sp

        if microcom & MASKS[IP_OUT]:
            if self.oper_r != 0:
                assert 'Error, someone already wrote to bus'
            self.oper_r = self.ip

        if microcom & MASKS[DATA_OUTPUT]:
            if self.oper_r != 0:
                assert 'Error, someone already wrote to bus'
            self.oper_r = self.data

    def write_out(self, microcom):
        if microcom & MASKS[CR_INPUT]:
            self.cr = self.out & 0xFFFF

        if microcom & MASKS[RA_INPUT]:
            self.ra = self.out

        if microcom & MASKS[RB_INPUT]:
            self.rb = self.out

        if microcom & MASKS[RC_INPUT]:
            self.rc = self.out

        if microcom & MASKS[RD_INPUT]:
            self.rd = self.out

        if microcom & MASKS[IP_INPUT]:
            self.ip = self.out & 0x7F

        if microcom & MASKS[SP_INPUT]:
            self.sp = self.out & 0x7F

        if microcom & MASKS[BUF_INPUT]:
            self.buf = self.out

        if microcom & MASKS[DATA_INPUT]:
            self.data = self.out

        if microcom & MASKS[ADDR_INPUT]:
            self.addr = self.out & 0x7F

    def use_alu(self, microcom: int):
        tmp_out: int = 0
        tmp_oper_l: int = self.oper_l
        tmp_oper_r: int = self.oper_r

        if microcom & MASKS[ALU_LEFT_INV]:
            tmp_oper_l = ~tmp_oper_l

        if microcom & MASKS[ALU_RIGHT_INV]:
            tmp_oper_r = ~tmp_oper_r

        if microcom & MASKS[ALU_INC]:
            tmp_oper_r = tmp_oper_r + 1

        if microcom & MASKS[ALU_SUM]:
            tmp_out = tmp_oper_l + tmp_oper_r

        if microcom & MASKS[ALU_MUL]:
            tmp_out = tmp_oper_l * tmp_oper_r

        if microcom & MASKS[ALU_DIV]:
            if tmp_oper_r != 0:
                tmp_out = tmp_oper_l // tmp_oper_r
                self.rd = (tmp_oper_l % tmp_oper_r) & 0xFFFFFFFF
            else:
                self.ps |= PS_C_FLAG

        if microcom & MASKS[SET_C]:
            if 0x100000000 & tmp_out:
                self.ps |= PS_C_FLAG
            else:
                self.ps &= ~PS_C_FLAG

        if microcom & MASKS[SET_V]:
            if (0x80000000 & tmp_oper_l) and (0x80000000 & tmp_oper_r) and not (0x80000000 & tmp_out):
                self.ps |= PS_V_FLAG
            elif not (0x80000000 & tmp_oper_l) and not (0x80000000 & tmp_oper_r) and (0x80000000 & tmp_out):
                self.ps |= PS_V_FLAG
            else:
                self.ps &= ~PS_V_FLAG

        if microcom & MASKS[SET_N]:
            if 0x80000000 & tmp_out:
                self.ps |= PS_N_FLAG
            else:
                self.ps &= ~PS_N_FLAG

        if microcom & MASKS[SET_Z]:
            if tmp_out == 0:
                self.ps |= PS_Z_FLAG
            else:
                self.ps &= ~PS_Z_FLAG

        self.alu_to_com = tmp_out

    def use_comm(self, microcom: int):
        tmp_val: int = self.alu_to_com
        tmp_out: int = 0

        if microcom & MASKS[COM_SHIFT_L]:
            tmp_out = tmp_val << 1

        if microcom & MASKS[COM_SHIFT_L_CYCLE]:
            tmp_out = (tmp_val << 1) | ((tmp_val & 0x80000000) >> 32)

        if microcom & MASKS[COM_SHIFT_R]:
            tmp_out = tmp_val >> 1

        if microcom & MASKS[COM_SHIFT_R_CYCLE]:
            tmp_out = (tmp_val >> 1) | ((tmp_val & 0x00000001) << 32)

        if microcom & MASKS[COM_H_TO_H]:
            tmp_out |= tmp_val & 0xFFFFFF00

        if microcom & MASKS[COM_L_TO_L]:
            tmp_out |= tmp_val & 0x000000FF

        if microcom & MASKS[COM_H_TO_L]:
            tmp_out |= (tmp_val & 0xFF00) >> 8

        if microcom & MASKS[COM_L_TO_H]:
            tmp_out |= (tmp_val & 0x00FF) << 8

        self.out = (tmp_out & 0xFFFFFFFF)

    def execute_operational_mc(self, microcom: int):
        if len(self.input_queue) > 0:
            self.ps |= PS_INPUT_R_FLAG
        else:
            self.ps &= ~PS_INPUT_R_FLAG

        self.use_data_input(microcom)
        self.fill_oper_l(microcom)
        self.fill_oper_r(microcom)
        self.use_alu(microcom)
        self.use_comm(microcom)
        self.write_out(microcom)
        self.use_data_output(microcom)
        if microcom & MASKS[HALT]:
            self.ps &= ~PS_IS_WORKING
        self.oper_l = 0
        self.oper_r = 0

    def start(self):
        self.ps |= PS_IS_WORKING
        self.ip = 0


class ProcessorController:
    def __init__(self):
        self.control_unit = ControlUnit()

        # Поток трассировки
        self.trace = io.StringIO()

    def load_programm(self, filename: str) -> {int, int}:

        with open(filename, 'r') as file:
            code = json.load(file)

        commands_num: int = 0
        datas_num: int = 0
        for instructs in code:
            if instructs['mem'] == "command":
                self.control_unit.proc.command_mem[instructs['address']] = (int(instructs['command'], 16))
                commands_num += 1
            else:
                self.control_unit.proc.data_mem[instructs['address']] = int(instructs['data'])
                datas_num += 1
        return commands_num, datas_num

    def write_input(self, input: str):
        for ch in input:
            self.control_unit.proc.input_queue.append(ord(ch))

    def write_input_int(self, input: int):
        self.control_unit.proc.input_queue.append(input)

    def get_one_output(self):
        return self.control_unit.proc.out_queue.pop()

    def get_all_output(self):
        return self.control_unit.proc.out_queue

    def work(self) -> {int, int}:
        self.control_unit.ip = 0
        self.control_unit.proc.start()
        self.control_unit.proc.out_queue.clear()
        self.trace = io.StringIO()
        self.trace.write('| cr | ra | rb | rc | rd | ip | sp | ps | buf | data | addr | control unit ip |\n'
                         '|----|----|----|----|----|----|----|----|-----|------|------|-----------------|\n')

        self.write_trace()

        num_of_ticks: int = 0
        num_of_commands: int = 0

        while self.control_unit.is_processor_working():
            if self.control_unit.ip == 0:
                num_of_commands += 1

            self.control_unit.tick()
            num_of_ticks += 1
            self.write_trace()

        return num_of_ticks, num_of_commands

    def get_trace(self) -> io.StringIO:
        return self.trace

    def write_trace(self):
        # | cr | ra | rb | rc | rd | ip | sp | ps | buf | data | addr | control unit ip |
        self.trace.write(f"| {hex(self.control_unit.proc.cr)} "
                         f"| {hex(self.control_unit.proc.ra)} "
                         f"| {hex(self.control_unit.proc.rb)} "
                         f"| {hex(self.control_unit.proc.rc)} "
                         f"| {hex(self.control_unit.proc.rd)} "
                         f"| {hex(self.control_unit.proc.ip)} "
                         f"| {hex(self.control_unit.proc.sp)} "
                         f"| {hex(self.control_unit.proc.ps)} "
                         f"| {hex(self.control_unit.proc.buf)} "
                         f"| {hex(self.control_unit.proc.data)} "
                         f"| {hex(self.control_unit.proc.addr)} "
                         f"| {hex(self.control_unit.ip)} |\n")


if __name__ == '__main__':
    parse: argparse.ArgumentParser = argparse.ArgumentParser()
    parse.add_argument('program')
    parse.add_argument('input_file')
    parse.add_argument('out_file')
    args = parse.parse_args()
    proc: ProcessorController = ProcessorController()
    commands_num, datas_num = proc.load_programm(args.program)
    with open(args.input_file, 'r') as file:
        for line in file.readlines():
            if is_int(line):
                proc.write_input_int(int(line, 0))
            else:
                proc.write_input(line)

    ticks, commands = proc.work()
    with open(args.out_file, 'w') as file:
        file.write(f'Program completed, output: {", ".join([str(num) for num in proc.get_all_output()])}, ticks: {ticks}, commands: {commands}, in programm {commands_num} commands, and {datas_num} datas.\nTrace:\n\n')
        file.write(proc.get_trace().getvalue())
