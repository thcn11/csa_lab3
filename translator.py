import argparse
import json

from common_functions import *


commands = ['MOV', 'LD', 'SV',
            'ADD', 'SUB', 'MUL',
            'DIV', 'INC', 'DEC',
            'TEST', 'JMP', 'JZ',
            'JN', 'JV', 'JR', 'CALL',
            'RET', 'PUSH', 'POP',
            'HALT', 'READ', 'WRITE']

data_commands = ['NUM', 'CHAR']


def assert_error(message):
    print(message)
    exit(1)


def find_in_row(line: list[str], part: str) -> int:
    for num in range(len(line)):
        if line[num] == part:
            return num
    return -1


def translate(input_file: str, output_file: str):
    file_lines: list = []

    with open(input_file) as file:
        for line in file.readlines():
            line = line.split(';', 1)[0]
            if line.strip() != '':
                striped_line = line.strip()
                file_lines.append(striped_line.split())

    address: int = 0
    command_on_address: dict = {}
    labels: dict = {}
    datas: list = []
    for line in file_lines:

        if line[0][-1] == ':' or (len(line) > 1 and (line[1] == ':' or line[1][0] == ':')):
            if line[0][-1] == ':':
                labels[line[0][0:-1]] = address
            else:
                labels[line[0]] = address
        elif 'ORG' in line:
            org_num = find_in_row(line, 'ORG')
            if org_num == -1:
                assert_error('Unable error')

            if len(line) >= org_num + 1:
                assert_error('Forget num for org')

            address = int(line[org_num + 1])
        elif any(com in line for com in commands):
            for com in commands:
                place = find_in_row(line, com)
                if place != -1:
                    command_on_address[address] = line, com, place
                    break
            address += 1
        elif any(dat in line for dat in data_commands):
            for dat in data_commands:
                place = find_in_row(line, dat)
                if place != -1:
                    datas.append([line, place])
                    break
        else:
            assert_error(f'Unknown command on line {" ".join(line)}')

    result: list = []

    for com_num in command_on_address:
        com, com_name, com_place = command_on_address[com_num]
        if com_name == 'MOV':

            if len(com) <= com_place + 2:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("01XX")}
            arg1: str = com[com_place + 1]
            arg2: str = com[com_place + 2]
            if arg1 == 'ra':
                tmp_dict["command"][2] = '1'
            elif arg1 == 'rb':
                tmp_dict["command"][2] = '2'
            elif arg1 == 'rc':
                tmp_dict["command"][2] = '4'
            elif arg1 == 'rd':
                tmp_dict["command"][2] = '8'
            else:
                assert_error('Invalid argument')

            if arg2 == 'ra':
                tmp_dict["command"][3] = '1'
            if arg2 == 'rb':
                tmp_dict["command"][3] = '2'
            if arg2 == 'rc':
                tmp_dict["command"][3] = '4'
            if arg2 == 'rd':
                tmp_dict["command"][3] = '8'

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)

        elif com_name == 'LD':
            if len(com) <= com_place + 2:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("1XXX")}
            arg1: str = com[com_place + 1]
            arg2: str = com[com_place + 2]
            if arg1 == 'ra':
                tmp_dict["command"][1] = '1'
            if arg1 == 'rb':
                tmp_dict["command"][1] = '2'
            if arg1 == 'rc':
                tmp_dict["command"][1] = '4'
            if arg1 == 'rd':
                tmp_dict["command"][1] = '8'

            int_arg2: int = 0
            if is_int(arg2):
                int_arg2 = int(arg2, 0)
                if int_arg2 > 0x7F:
                    assert_error('Address is too big')
                if int_arg2 < 0:
                    assert_error('Address can not br negative')
            else:
                assert_error('Error, argument should be num LD')

            if int_arg2 <= 0xF:
                tmp_dict['command'][2] = '0'
                tmp_dict['command'][3] = hex(int_arg2)[2]
            else:
                tmp_dict['command'][2] = hex(int_arg2)[2]
                tmp_dict['command'][3] = hex(int_arg2)[3]

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)

        elif com_name == 'SV':
            if len(com) <= com_place + 2:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("2XXX")}
            arg1: str = com[com_place + 1]
            arg2: str = com[com_place + 2]
            if arg1 == 'ra':
                tmp_dict["command"][1] = '1'
            if arg1 == 'rb':
                tmp_dict["command"][1] = '2'
            if arg1 == 'rc':
                tmp_dict["command"][1] = '4'
            if arg1 == 'rd':
                tmp_dict["command"][1] = '8'

            int_arg2: int = 0
            if is_int(arg2):
                int_arg2 = int(arg2, 0)
                if int_arg2 > 0x7F:
                    assert_error('Address is too big')
                if int_arg2 < 0:
                    assert_error('Address can not br negative')
            else:
                assert_error('Error, argument should be num')

            if int_arg2 <= 0xF:
                tmp_dict['command'][2] = '0'
                tmp_dict['command'][3] = hex(int_arg2)[2]
            else:
                tmp_dict['command'][2] = hex(int_arg2)[2]
                tmp_dict['command'][3] = hex(int_arg2)[3]

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)

        elif com_name == 'ADD':
            if len(com) <= com_place + 2:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("02XX")}
            arg1 = com[com_place + 1]
            arg2 = com[com_place + 2]
            if arg1 == 'ra':
                tmp_dict["command"][2] = '1'
            if arg1 == 'rb':
                tmp_dict["command"][2] = '2'
            if arg1 == 'rc':
                tmp_dict["command"][2] = '4'
            if arg1 == 'rd':
                tmp_dict["command"][2] = '8'

            if arg2 == 'ra':
                tmp_dict["command"][3] = '1'
            if arg2 == 'rb':
                tmp_dict["command"][3] = '2'
            if arg2 == 'rc':
                tmp_dict["command"][3] = '4'
            if arg2 == 'rd':
                tmp_dict["command"][3] = '8'

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)
        elif com_name == 'SUB':
            if len(com) <= com_place + 2:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("03XX")}
            arg1: str = com[com_place + 1]
            arg2: str = com[com_place + 2]
            if arg1 == 'ra':
                tmp_dict["command"][2] = '1'
            if arg1 == 'rb':
                tmp_dict["command"][2] = '2'
            if arg1 == 'rc':
                tmp_dict["command"][2] = '4'
            if arg1 == 'rd':
                tmp_dict["command"][2] = '8'

            if arg2 == 'ra':
                tmp_dict["command"][3] = '1'
            if arg2 == 'rb':
                tmp_dict["command"][3] = '2'
            if arg2 == 'rc':
                tmp_dict["command"][3] = '4'
            if arg2 == 'rd':
                tmp_dict["command"][3] = '8'

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)
        elif com_name == 'MUL':
            if len(com) <= com_place + 2:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("04XX")}
            arg1: str = com[com_place + 1]
            arg2: str = com[com_place + 2]
            if arg1 == 'ra':
                tmp_dict["command"][2] = '1'
            if arg1 == 'rb':
                tmp_dict["command"][2] = '2'
            if arg1 == 'rc':
                tmp_dict["command"][2] = '4'
            if arg1 == 'rd':
                tmp_dict["command"][2] = '8'

            if arg2 == 'ra':
                tmp_dict["command"][3] = '1'
            if arg2 == 'rb':
                tmp_dict["command"][3] = '2'
            if arg2 == 'rc':
                tmp_dict["command"][3] = '4'
            if arg2 == 'rd':
                tmp_dict["command"][3] = '8'

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)
        elif com_name == 'DIV':
            if len(com) <= com_place + 2:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("05XX")}
            arg1: str = com[com_place + 1]
            arg2: str = com[com_place + 2]
            if arg1 == 'ra':
                tmp_dict["command"][2] = '1'
            if arg1 == 'rb':
                tmp_dict["command"][2] = '2'
            if arg1 == 'rc':
                tmp_dict["command"][2] = '4'
            if arg1 == 'rd':
                tmp_dict["command"][2] = '8'

            if arg2 == 'ra':
                tmp_dict["command"][3] = '1'
            if arg2 == 'rb':
                tmp_dict["command"][3] = '2'
            if arg2 == 'rc':
                tmp_dict["command"][3] = '4'
            if arg2 == 'rd':
                tmp_dict["command"][3] = '8'

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)
        elif com_name == 'INC':
            if len(com) <= com_place + 1:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("060X")}
            arg1: str = com[com_place + 1]
            if arg1 == 'ra':
                tmp_dict["command"][3] = '1'
            if arg1 == 'rb':
                tmp_dict["command"][3] = '2'
            if arg1 == 'rc':
                tmp_dict["command"][3] = '4'
            if arg1 == 'rd':
                tmp_dict["command"][3] = '8'

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)

        elif com_name == 'DEC':
            if len(com) <= com_place + 1:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("070X")}
            arg1: str = com[com_place + 1]
            if arg1 == 'ra':
                tmp_dict["command"][3] = '1'
            if arg1 == 'rb':
                tmp_dict["command"][3] = '2'
            if arg1 == 'rc':
                tmp_dict["command"][3] = '4'
            if arg1 == 'rd':
                tmp_dict["command"][3] = '8'

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)

        elif com_name == 'TEST':
            if len(com) <= com_place + 1:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("080X")}
            arg1: str = com[com_place + 1]
            if arg1 == 'ra':
                tmp_dict["command"][3] = '1'
            if arg1 == 'rb':
                tmp_dict["command"][3] = '2'
            if arg1 == 'rc':
                tmp_dict["command"][3] = '4'
            if arg1 == 'rd':
                tmp_dict["command"][3] = '8'

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)

        elif com_name == 'JMP':
            if len(com) <= com_place + 1:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("40XX")}
            arg1: str = com[com_place + 1]

            int_arg1: int = 0
            if is_int(arg1):
                int_arg1 = int(arg1, 0)
                if int_arg1 > 0x7F:
                    assert_error('Address is too big')
                if int_arg1 < 0:
                    assert_error('Address can not br negative')
            else:
                if arg1 in labels:
                    int_arg1 = labels[arg1]
                else:
                    assert_error('Error, wrong label')

            if int_arg1 <= 0xF:
                tmp_dict['command'][2] = '0'
                tmp_dict['command'][3] = hex(int_arg1)[2]
            else:
                tmp_dict['command'][2] = hex(int_arg1)[2]
                tmp_dict['command'][3] = hex(int_arg1)[3]

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)

        elif com_name == 'JZ':
            if len(com) <= com_place + 1:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("41XX")}
            arg1: str = com[com_place + 1]

            int_arg1: int = 0
            if is_int(arg1):
                int_arg1 = int(arg1, 0)
                if int_arg1 > 0x7F:
                    assert_error('Address is too big')
                if int_arg1 < 0:
                    assert_error('Address can not br negative')
            else:
                if arg1 in labels:
                    int_arg1 = labels[arg1]
                else:
                    assert_error('Error, wrong label')

            if int_arg1 <= 0xF:
                tmp_dict['command'][2] = '0'
                tmp_dict['command'][3] = hex(int_arg1)[2]
            else:
                tmp_dict['command'][2] = hex(int_arg1)[2]
                tmp_dict['command'][3] = hex(int_arg1)[3]

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)

        elif com_name == 'JN':
            if len(com) <= com_place + 1:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("42XX")}
            arg1: str = com[com_place + 1]

            int_arg1: int = 0
            if is_int(arg1):
                int_arg1 = int(arg1, 0)
                if int_arg1 > 0x7F:
                    assert_error('Address is too big')
                if int_arg1 < 0:
                    assert_error('Address can not br negative')
            else:
                if arg1 in labels:
                    int_arg1 = labels[arg1]
                else:
                    assert_error('Error, wrong label')

            if int_arg1 <= 0xF:
                tmp_dict['command'][2] = '0'
                tmp_dict['command'][3] = hex(int_arg1)[2]
            else:
                tmp_dict['command'][2] = hex(int_arg1)[2]
                tmp_dict['command'][3] = hex(int_arg1)[3]

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)

        elif com_name == 'JV':
            if len(com) <= com_place + 1:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("44XX")}
            arg1: str = com[com_place + 1]

            int_arg1: int = 0
            if is_int(arg1):
                int_arg1 = int(arg1, 0)
                if int_arg1 > 0x7F:
                    assert_error('Address is too big')
                if int_arg1 < 0:
                    assert_error('Address can not br negative')
            else:
                if arg1 in labels:
                    int_arg1 = labels[arg1]
                else:
                    assert_error('Error, wrong label')

            if int_arg1 <= 0xF:
                tmp_dict['command'][2] = '0'
                tmp_dict['command'][3] = hex(int_arg1)[2]
            else:
                tmp_dict['command'][2] = hex(int_arg1)[2]
                tmp_dict['command'][3] = hex(int_arg1)[3]

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)

        elif com_name == 'JR':
            if len(com) <= com_place + 1:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("48XX")}
            arg1: str = com[com_place + 1]

            int_arg1: int = 0
            if is_int(arg1):
                int_arg1 = int(arg1, 0)
                if int_arg1 > 0x7F:
                    assert_error('Address is too big')
                if int_arg1 < 0:
                    assert_error('Address can not br negative')
            else:
                if arg1 in labels:
                    int_arg1 = labels[arg1]
                else:
                    assert_error('Error, wrong label')

            if int_arg1 <= 0xF:
                tmp_dict['command'][2] = '0'
                tmp_dict['command'][3] = hex(int_arg1)[2]
            else:
                tmp_dict['command'][2] = hex(int_arg1)[2]
                tmp_dict['command'][3] = hex(int_arg1)[3]

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)

        elif com_name == 'CALL':
            if len(com) <= com_place + 1:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("80XX")}
            arg1: str = com[com_place + 1]

            int_arg1: int = 0
            if is_int(arg1):
                int_arg1 = int(arg1, 0)
                if int_arg1 > 0x7F:
                    assert_error('Address is too big')
                if int_arg1 < 0:
                    assert_error('Address can not br negative')
            else:
                if arg1 in labels:
                    int_arg1 = labels[arg1]
                else:
                    assert_error('Error, wrong label')

            if int_arg1 <= 0xF:
                tmp_dict['command'][2] = '0'
                tmp_dict['command'][3] = hex(int_arg1)[2]
            else:
                tmp_dict['command'][2] = hex(int_arg1)[2]
                tmp_dict['command'][3] = hex(int_arg1)[3]

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)

        elif com_name == 'RET':

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": "8100"}

            result.append(tmp_dict)

        elif com_name == 'PUSH':
            if len(com) <= com_place + 1:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("820X")}
            arg1: str = com[com_place + 1]
            if arg1 == 'ra':
                tmp_dict["command"][3] = '1'
            if arg1 == 'rb':
                tmp_dict["command"][3] = '2'
            if arg1 == 'rc':
                tmp_dict["command"][3] = '4'
            if arg1 == 'rd':
                tmp_dict["command"][3] = '8'

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)

        elif com_name == 'POP':
            if len(com) <= com_place + 1:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("840X")}
            arg1: str = com[com_place + 1]
            if arg1 == 'ra':
                tmp_dict["command"][3] = '1'
            if arg1 == 'rb':
                tmp_dict["command"][3] = '2'
            if arg1 == 'rc':
                tmp_dict["command"][3] = '4'
            if arg1 == 'rd':
                tmp_dict["command"][3] = '8'

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)

        elif com_name == 'HALT':
            tmp_dict: dict = {"mem": "command", "address": com_num, "command": "0000"}

            result.append(tmp_dict)
        elif com_name == 'READ':
            if len(com) <= com_place + 1:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("001X")}
            arg1: str = com[com_place + 1]
            if arg1 == 'ra':
                tmp_dict["command"][3] = '1'
            if arg1 == 'rb':
                tmp_dict["command"][3] = '2'
            if arg1 == 'rc':
                tmp_dict["command"][3] = '4'
            if arg1 == 'rd':
                tmp_dict["command"][3] = '8'

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)

        elif com_name == 'WRITE':
            if len(com) <= com_place + 1:
                assert_error('Not enough arguments in command MOV')

            tmp_dict: dict = {"mem": "command", "address": com_num, "command": list("002X")}
            arg1: str = com[com_place + 1]
            if arg1 == 'ra':
                tmp_dict["command"][3] = '1'
            if arg1 == 'rb':
                tmp_dict["command"][3] = '2'
            if arg1 == 'rc':
                tmp_dict["command"][3] = '4'
            if arg1 == 'rd':
                tmp_dict["command"][3] = '8'

            tmp_dict["command"] = "".join(tmp_dict["command"])
            result.append(tmp_dict)

    for data in datas:
        if data[0][data[1]] == 'CHAR':
            if len(data) < data[1] + 2:
                assert_error('Error, CHAR needs 2 args')

            address: str = data[0][data[1] + 1]
            data: str = data[0][data[1] + 2]

            tmp_dict: dict = {'mem': 'data'}

            if is_int(address):
                int_address: int = int(address, 0)
                if int_address < 0:
                    assert_error('Error, Address for CHAR should be >= 0')
                if int_address > 0xFF:
                    assert_error('Error, Address for CHAR should be <= 0xFF')
                tmp_dict['address'] = int(address, 0)
            else:
                assert_error('Error, CHAR address need to be num')

            if len(data) > 1:
                assert_error('Error, data should be one char')

            tmp_dict['data'] = ord(data)
            result.append(tmp_dict)

        elif data[0][data[1]] == 'NUM':
            if len(data) < data[1] + 2:
                assert_error('Error, NUM need 2 args')

            address: str = data[0][data[1] + 1]
            data: str = data[0][data[1] + 2]

            tmp_dict: dict = {'mem': 'data'}

            if is_int(address):
                int_address: int = int(address, 0)
                if int_address < 0:
                    assert_error('Error, Address for NUM should be >= 0')
                if int_address > 0xFF:
                    assert_error('Error, Address for NUM should be <= 0xFF')
                tmp_dict['address'] = int_address
            else:
                assert_error('Error, NUM address need to be num')

            if is_int(data):
                int_data: int = int(data, 0)
                if int_data < 0:
                    assert_error('Error, Value for NUM should be >= 0')
                if int_data > 0xFFFFFFFF:
                    assert_error('Error, Value for NUM should be <= 0xFFFFFFFF')
                tmp_dict['data'] = int_data
            else:
                assert_error('Error, data should be num')

            result.append(tmp_dict)

    with open(output_file, 'w') as output_file:
        json.dump(result, output_file)


if __name__ == '__main__':
    parse: argparse.ArgumentParser = argparse.ArgumentParser()
    parse.add_argument('input_filename')
    parse.add_argument('output_filename')
    args = parse.parse_args()
    translate(args.input_filename, args.output_filename)
    print('Translated')
