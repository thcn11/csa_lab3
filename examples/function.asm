NUM 0 1
NUM 1 2
NUM 2 3


CALL func1 ;should write 1
CALL func2 ;should write 2
CALL func3 ;should write 3
CALL func_with_stack ;should write 2 1
HALT

func1:
    LD ra 0
    WRITE ra
    RET
func2:
    LD ra 1
    WRITE ra
    RET
func3:
    LD ra 2
    WRITE ra
    RET
func_with_stack:
    LD ra 0
    PUSH ra
    LD ra 1
    WRITE ra
    POP ra
    WRITE ra
    RET
