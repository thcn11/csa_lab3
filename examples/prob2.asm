NUM 0 1 ;first number
NUM 1 2 ;second number
NUM 2 4000000
NUM 3 2 ;summary of even
NUM 4 2 ;const

LOOP:
LD ra 0
LD rc 1
ADD ra rc
SV ra 1
SV rc 0
COMPARE:
LD rb 2
SUB rb ra
JN TEST_ODD
JMP END
TEST_ODD:
LD rc 4
MOV ra rb
DIV rc rb
TEST rd
JZ SUM
JMP LOOP
SUM:
LD rb 3
ADD rb ra
SV rb 3
JMP LOOP
END:
LD ra 3
WRITE ra
HALT
